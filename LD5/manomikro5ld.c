/*
  Mikrovaldiklių sistemos
  Laboratorinis darbas nr. 5
  Variantas 5.3
*/
// Declare library


//#define F_CPU 16000000UL   // Not necessary in simulator
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

// Define your global variables here
char mas[17] ="VilniusTech     "; // Teksto masyvas
char c; // Kintamasis, saugantis simbolį

// Prievadų ir išvadų aprašymas
#define LCD_Port         PORTD	// Apibreziamas LCD prievadas (PORTA, PORTB, PORTC, PORTD)
#define LCD_DPin         DDRD	// Apibreziami 4-bitu isvadai (PD4-PD7 at PORT D)
#define LCD_DATA0_PIN    PD4    // < pin for 4bit data bit 0  
#define LCD_DATA1_PIN    PD5    // < pin for 4bit data bit 1  
#define LCD_DATA2_PIN    PD6    // < pin for 4bit data bit 2  
#define LCD_DATA3_PIN    PD7    // < pin for 4bit data bit 3  
#define RSPIN 			 PD3	// RS Pin
#define ENPIN            PD2 	// E Pin
int runtime;			 // Laiktrodis (timer) LCD ekranui

// LCD inicializavimo funkcija su minimaliu komandų sąrašu
void LCD_Init (void)
{
    // Duomenu ir kontrolės išvadų nustatymas kaip išejimo
    LCD_DPin |= (1<<LCD_DATA0_PIN)|(1<<LCD_DATA1_PIN)|(1<<LCD_DATA2_PIN)|(1<<LCD_DATA3_PIN)|(1<<RSPIN)|(1<<ENPIN);		//Kontroles LCD isvadai (D4-D7)
	_delay_ms(16);		// 16 ms vėlinimo funkcija
  
                            
	LCD_Action(0x02);	    // Gražina žymeklį į pradinę padėtį (Adresas 0). Gražina ekraną į pirminę būseną jeigu ji buvo pakeista 
	LCD_Action(0x28);       // Duomenų išsiuntimas/gavimas 4 bitu ilgiu (DB7-DB4) 
	LCD_Action(0x0C);       // Ekranas įjungtas, žymeklis išjungtas 
	LCD_Action(0x06);       // Žymeklio poslinkis į dešinę
	LCD_Action(0x01);       // Išvalomas LCD 
	_delay_ms(5);           // 5 ms vėlinimo funkcija
}

// Komandų ir duomenų siuntimo funkcija LCD ekranui
void LCD_Action( unsigned char cmnd )
{   
	LCD_Port = (LCD_Port & 0x0F) | (cmnd & 0xF0); 
	LCD_Port &= ~ (1<<RSPIN); 
	LCD_Port |= (1<<ENPIN); // Pateikiamas aukštas impulsas (LCD ON)
	_delay_us(1); // 1 ms vėlinimo funkcija
	LCD_Port &= ~ (1<<ENPIN); // Pateikiamas žemas impulsas (LCD OFF)
	_delay_us(200); // Laukiama 200 mikrosekundziu
	LCD_Port = (LCD_Port & 0x0F) | (cmnd << 4); //?
	LCD_Port |= (1<<ENPIN); // Pateikiamas aukštas impulsas (LCD ON)
	_delay_us(1);  // 1 ms vėlinimo funkcija
	LCD_Port &= ~ (1<<ENPIN); // Pateikiamas žemas impulsas (LCD OFF)
	_delay_ms(2);  // 2 ms vėlinimo funkcija
}

// Išvalomas visas LCD ekranas
void LCD_Clear()
{
	LCD_Action (0x01);		// Išvalomas LCD ekranas
	_delay_ms(2);			// 2 ms vėlinimo funkcija
	LCD_Action (0x80);		// Nustatoma pozicija į 1 eilutę ir 1 stulpelį
}

// Atvaizduojamos simbolių eilutės
void LCD_Print (char *str)
{
	int i;
	for(i=0; str[i]!=0; i++) // siunčiamas kiekvienas eilutės simbolis kol sąlyga tenkinama
	{
		LCD_Port = (LCD_Port & 0x0F) | (str[i] & 0xF0); 
		LCD_Port |= (1<<RSPIN); 
		LCD_Port|= (1<<ENPIN); // Pateikiamas aukštas impulsas (LCD ON)
		_delay_us(1); // 1 us vėlinimo funkcija
		LCD_Port &= ~ (1<<ENPIN); // Pateikiamas žemas impulsas (LCD OFF)
		_delay_us(200); // 200 us vėlinimo funkcija
		LCD_Port = (LCD_Port & 0x0F) | (str[i] << 4); 
		LCD_Port |= (1<<ENPIN); // Pateikiamas aukštas impulsas (LCD ON)
		_delay_us(1); // 1 us vėlinimo funkcija
		LCD_Port &= ~ (1<<ENPIN); // Pateikiamas žemas impulsas (LCD OFF)
		_delay_ms(2); // 2 ms vėlinimo funkcija
	}
}

// Atvaizduojami simboliai tam tikroje vietoje
// eilutė - 0 arba 1
// pozicija - stulpelis nuo 0 iki 16
void LCD_Printpos (char row, char pos, char *str)
{
	if (row == 0 && pos<16)
	LCD_Action((pos & 0x0F)|0x80); // Pirmos eilutės padėtis
	else if (row == 1 && pos<16)
	LCD_Action((pos & 0x0F)|0xC0); // Antros eilutės padėtis
	LCD_Print(str); // Atvaizduojama simbolių eilutė
}


int main()
{
    int i, n;
	LCD_Init();	// Inicializuojamas LCD ekranas
	LCD_Printpos(0, 2, "VilniusTech"); // Atvaizduojami nurodyti simboliai pirmoje eilutėje
	LCD_Action(0x80); // Perejimas i antra eilute
  
  
	while(1) {
     for (i=0; i < 16; i++) 
      {
       LCD_Printpos(1,0, mas); // Atvaizduojami nurodyti simboliai antroje eilutėje
       c = mas[0]; // Paimamas pirmas simbolis iš kairės
       // Simbolių pastumimas (po vieną į kairę pusę) 
         for (n = 0; n < 16; n++) 
         {
          mas[n] = mas[n + 1]; // Naujo simbolio išsaugojimas
         }
       
       mas[15] = c; // Išsaugotas simbolis perkeliamas į galą
        
       mas[16] = 0; // Eilutės žymeklio pabaiga
	  }
   }
}