/*
  Mikrovaldiklių sistemos
  Laboratorinis darbas nr. 3
  Variantas 3.3
*/
// Declare library
#include <avr/io.h> 
#include <util/delay.h>
#include <avr/interrupt.h>

// Globalus kintamasis, saugojantis persipildymo pertraukčių skaičių
int tot_overflow=0; 
// Saugoma sutapimo pertraukčių B kanale skaičius
//int compa=0;

// Laikmačio/skaitiklio1 pertraukties funkcija, kuri vykdoma atsiradus persipidymo
// pertraukčiai laikmačio/skaitiklio1
ISR(TIMER1_OVF_vect) 
{
  PORTC^=(1<<PC0); // LED PC0 šviečia
  tot_overflow++; // Didinama kintamojo reikšmė vienetu
}

// Laikmačio/skaitiklio2 pertraukties funkcija, kuri vykdoma atsiradus persipidymo
// pertraukčiai laikmačio/skaitiklio2
ISR(TIMER2_COMPA_vect) 
{
  PORTC^=(1<<PC1); // LED PC1 šviečia
  tot_overflow++; // Didinama kintamojo reikšmė vienetu
}


void timer1_init() // Laikmačio/skaitiklio1 inicializavimas
{  
  //TCCR1A = 0; // Nunulinamas TCCR1A registras
  //TCCR1B = 0; // Nunulinamas TCCR1B registras
  TCNT1=0; // Nunulinama laikmačio/skaitiklio1 registro reikšmė 
  //TCCR1A|=(1<<WGM12); // Įjungiamas CTC režimas 
  TCCR1B|=(1<<CS12); // 256 daliklis
  TIMSK1|=(1<<TOIE1); // Įgalinama laikmačio/skaitiklio1 persipildymo pertrauktis
} 
/*
// Laikmačio/skaitiklio2 inicializavimas
void timer2_init() 
{ 
  TCNT2=0; // Nunulinama laikmačio/skaitiklio2 registro reikšmė 
  TCCR2B|=(1<<CS22); // 64 daliklis
  TIMSK2|=(1<<TOIE2); // Įgalinama laikmačio/skaitiklio2 persipildymo pertrauktis
} 
*/
int main(void){ 
  Serial.begin(9600); // Duomenų perdavimo sparta (bit/s) į terminalą
  DDRC|=(1<<PC1)|(1<<PC0); // Raudoni LED nustatomi kaip išėjimo išvadai
 
  timer1_init(); // Inicializuojamas laikmatis/skaitiklis1 
  //timer2_init(); // Nnicializuojamas laikmatis/skaitiklis2
  sei(); // Įgalinamos globalios pertrauktis
  
  while(1){   // ciklo pradžia
   
    // Laikmačio/skaitiklio1 duomenų išvedimas į terminalą
    Serial.println(TCNT1);
    // Laikmačio/skaitiklio2 duomenų išvedimas į terminalą
    //Serial.print("compa: ");
    //Serial.println(compa);
  
	} // ciklo pabaiga
    
}
