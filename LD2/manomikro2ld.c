/*
  Mikrovaldiklių sistemos
  Laboratorinis darbas nr. 2
  Variantas 2.3
  1 užduotis
*/
// Declare library

#include <avr/io.h> 
#include <util/delay.h>
#include <avr/interrupt.h>
/*
int main(void){ 

   DDRB|=(1<<PB5);  // PB5 nustatomas kaip išėjimo išvadas
   DDRD&=~(1<<PD7); // PD7 nustatomas kaip įėjimo išvadas į mygtuką
   PORTD|=(1<<PD7); // Mygtukui įjungiamas pull-up rezistorius
  
  while(1){ // Ciklo pradžia
    
  	if(PIND&(1<<PD7)) // Stebimas PD7 išvado signalo lygis 
    {  
      PORTB|=(1<<PB5);  
    }
    else
    {
      PORTB&=~(1<<PB5); 
    }
  } // Ciklo pabaiga
}

*/


//2 užduotis

// Declare library

#include <avr/io.h> 
#include <util/delay.h>
#include <avr/interrupt.h>

// Aprašomi globalūs kintamieji 
int number=0; // Kintamasis duomenų skaičiavimui.
int data[4]={0b000001, 0b000010, 0b000100, 0b000010}; // Kintamajam priskiriamos 4 dvejetainės reikšmės

// Konfiguruojama išorinė pertrauktis INT1
ISR(INT1_vect) // Išorinių pertraukčių išvadas PD3 (ISR - pertraukties programa)
{
  if(number < 3) 
  { 
    number=number+1; // Kol sąlyga tenkinama, reikšmė padidinama vienetu
  } 
  else 
  {
    number=0; // Jei sąlyga netenkinama, reikšmė priskiriama nuliui
  }
  PORTC=data[number]; // Pasiunčiami duomenis į raudonus led
}


int main(void){ 
  
  DDRC|=(1<<PC2)|(1<<PC1)|(1<<PC0);// Raudoni LED nustatomi kaip išėjimo išvadai
  DDRD&=~(1<<PD3); // PD3 nustatomas kaip įėjimo išvadas į mygtuką
  PORTD|=(1<<PD3); // Mygtukui įjungiamas pull-up rezistorius
  
  EICRA|=(1<<ISC11)|(1<<ISC10); // Nustatomas kylantis frontas išorinei pertraukčiai INT1
  
  EIMSK|=(1<<INT1); // Kaukės registras (įgalina pertrauktį INT1)
 
  SREG|=(1<<7); // Igalina globalias pertrauktis Bit7 SREG registrui 
 
  
  while(1){ // Ciklo pradžia  
  
    PORTC=data[number]; // Reikšmės įstatomos į ciklą
  
    _delay_ms(250); // 250ms vėlinimo funkcija
	} // Ciklo pabaiga
}
