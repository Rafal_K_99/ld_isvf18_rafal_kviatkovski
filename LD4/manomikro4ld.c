/*
  Mikrovaldiklių sistemos
  Laboratorinis darbas nr. 4
  Variantas 4.3
  1 užduotis
*/
// Declare library

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

int tot_overflow=0; // Kintamasis, skirtas saugoti sutapimo pertraukčių skaičių A kanale
//int result=0; // Viršutinių 8 bitų saugojimo kintamasis

// Laikmačio/skaitiklio0 pertraukties funkcija, kuri vykdoma atsiradus persipidymo
// pertraukčiai laikmačio/skaitiklio0
ISR(TIMER0_COMPA_vect) 
{
  PORTD^=(1<<PD6); // Vaizdas oscilografe
  tot_overflow++; // Reikšmė didinama vienetu
}


//ISR(ADC_vect) 
//{
//  result=ADCH; // Viršutiniai 8 bitai išsaugomi kintamajame result
//}


void timer0_init() // Laikmačio/skaitiklio inicializavimas
{ 
  TCCR0A|=(1<<WGM02)|(1<<WGM00); // Nustatomas PWM režimas
  TCCR0B|=(1<<CS02)|(1<<CS00); // 1024 daliklis
  TCCR0A|=(1<<COM0A1)|(1<<COM0A0)|(1<<COM0B1)|(1<<COM0B0); 
  OCR0B = 4; // Nustatoma maksimali bitų reikšmė
  TCNT0=0; // Nunulinamas laikmačio/skaitiklio registras
} 

// Analoginio-skaitmeninio keitiklio inicializavimas

void adc0_init() 
{
  ADMUX|=(1<<REFS0); // Pasirenkama įtampa Vref (5V)
  ADMUX|=(1<<MUX2)|(1<<MUX0); // Pasirenkamas ADC5 kanalas
  ADMUX|=(1<<ADLAR); // Nustatomas lygiavimas iš kairės
  // ADC igalinimas 
  //ADCSRA|=(1<<ADEN);
  // Laikrodzio daliklio pasirinkimas 
  //ADCSRA|=(1<<ADPS0); // daliklis 2 
  ADCSRA = 0b10001111; // daliklis 2
}

int main(void){ 
   Serial.begin(9600); // Duomenų perdavimo sparta (bit/s)
 
  DDRD|=(1<<PD6)|(1<<PD5); // Išvadai PD6 ir PD5 nustatomi kaip išėjimo
 
  timer0_init(); // Laikmatis/skaitiklis0 
  
  adc0_init(); // Inicializuojamas analoginis-skaitmeninis keitiklis
  
  
  while(1){  // Ciklo pradžia

    ADCSRA|=(1<<ADSC); // Konvertavimo pradžia nustatant ADSC vėliavėle

    while(ADCSRA&(1<<ADSC)){ // Laukiama, kol konvertavimas bus baigtas
    
    // Duomenų išvedimas į terminalą
    //Serial.print("ADC: ");
    //Serial.println(result); 
    Serial.println(ADCH);
    }
   } 
}

// 2 užduotis

/*
#define F_CPU 20000000
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

double dutyCycle = 0; // Kintamasis, nusakantis procentinę laiko dalį, kai signalas buvo aukštame lygyje

void timer0_init() // Laikmačio 0 inicializavimas
{ 
  TCCR0A|=(1 << WGM01)|(1 << WGM00); // Nustatomas PWM režimas
  TCCR0B|=(1<<CS02)|(1<<CS00); // Daliklis 1024
  TIMSK0= (1 << TOIE0); // Nustatomas laikmačio/skaitiklio petraukties kaukės registras
  TCNT0=0; // Nunulinamas laikmačio/skaitiklio registras
  OCR0A = (dutyCycle/100)*255; // A kanalo palyginimo registrui priskiriama reikšmė 
  sei(); // Įgalinamos globalios pertrauktys
} 

// Laikmačio/skaitiklio0 pertraukties funkcija, kuri vykdoma atsiradus persipidymo
// pertraukčiai laikmačio/skaitiklio0

ISR(TIMER0_OVF_vect) 
{
  OCR0A = (dutyCycle/100)*255; 
  PORTD^=(1<<PD6); // OC0A
}


int main(void)
{ 
Serial.begin(9600); // Duomenų perdavimo sparta (bit/s)

DDRD|=(1<<PD6); // PD6 nustatomas kaip išėjimo išvadas
  
timer0_init(); // laikmačio/skaitiklio0 inicializavimas 
  
  while(1) // ciklo pradžia
  {  
   _delay_ms(100); // 100 ms vėlinimo funkcija
   dutyCycle=dutyCycle+10;; // Reikšmė padidinama kas 10%
    if(dutyCycle > 100){
    	if (TCNT0 > 10){

      		dutyCycle = 0; // Jei dutyCycle viršija 100%, reikšmė priskiriama nuliui
    	}
    }
    // Duomenų išvedimas į terminalą
  Serial.print("dutyCycle: ");
  Serial.println(dutyCycle); 
  Serial.println(OCR0A);
  } // ciklo pabaiga
} 


*/