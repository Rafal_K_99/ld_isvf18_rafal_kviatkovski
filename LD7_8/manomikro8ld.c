/* USER CODE BEGIN Header
 Laboratory works 7 and 8
*/

#include "main.h"

int main(void){
	
/* USER CODE BEGIN 1 */

RCC->AHB1ENR = 0x00000008; // Enable the clock for Port D
GPIOD->MODER = 0x15000000; // Set PIN12-14 of Port D as output
GPIOD->OTYPER = 0x00000000; // Set type of pin12-14 as push-pull
GPIOD->OSPEEDR = 0x2A000000; // Speed of pin12-14 Fast 50 MHz
GPIOD->PUPDR = 0x00000000; // No pull-up/down

/* USER CODE END 1 */
while (1) {

   // Set the pin GPIOD_Pin12-14 to logical high level
  GPIOD->ODR = 0x00001000; // 000
  GPIOD->ODR = 0x00002000; // 001
  GPIOD->ODR = 0x00003000; // 010
  GPIOD->ODR = 0x00004000; // 011
  GPIOD->ODR = 0x00005000; // 100
  GPIOD->ODR = 0x00006000; // 101
  GPIOD->ODR = 0x00007000; // 110

   // Reset the pin GPIOD_Pin12-14 to logical low level
  GPIOD->ODR &= ~((1 << 12)|(1 << 13)|(1 << 14)); // 111
  }
}