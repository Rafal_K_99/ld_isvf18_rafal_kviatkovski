/*
  Mikrovaldiklių sistemos
  Laboratorinis darbas nr. 1
  Variantas 1.3
  1-2 užduotys
*/

// Declare library
#include <avr/io.h>
#include <util/delay.h>

int main(void){
  
  DDRB=0b00111100; // Išvadai PB2-PB5 nustatomi kaip išėjimo
  PORTB|=(1<<PB1); // Įjungiamas pull-up rezistorius

  while(1){ // Ciklo pradžia 
    
    while(PINB&(1<<PB0)){} // Naujas begalinis ciklas sekos sustabdymui
    PORTB=(1<<PB5)|(1<<PB4)|(1<<PB3)|(1<<PB2); // Šviečia visi diodai: 1111 (15)
    _delay_ms(1000); // 1000 ms vėlinimo funkcija
    
    while(PINB&(1<<PB0)){}
    PORTB=(1<<PB5)|(1<<PB4)|(1<<PB3); // Šviečia pirmi trys diodai: 1110 (14)
    _delay_ms(1000); // 1000 ms vėlinimo funkcija
    
    while(PINB&(1<<PB0)){}
    PORTB=(1<<PB5)|(1<<PB4)|(1<<PB2); // Šviečia visi diodai isskyrus PB3: 1101 (13)
    _delay_ms(1000); // 1000 ms vėlinimo funkcija
    
    while(PINB&(1<<PB0)){}
    PORTB=(1<<PB5)|(1<<PB4); // Šviečia pirmi du diodai: 1100 (12)
    _delay_ms(1000); // 1000 ms vėlinimo funkcija
	} // Ciklo pabaiga
  }
