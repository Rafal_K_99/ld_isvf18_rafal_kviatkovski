/*
  Mikrovaldiklių sistemos
  Laboratorinis darbas nr. 6
  Variantas 6.3
*/
// Declare library

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include<stdio.h> 
#include<stdlib.h>
#include<string.h>

// Define your global variables here
char buf[50]; // char tipo masyvas skirtas temperatūros vertėms atvaizduoti
double temp=0; // pradinė temperatūros reikšmė

// Prievadų ir išvadų aprašymas
#define LCD_Port         PORTD	// Apibreziamas LCD prievadas (PORTA, PORTB, PORTC, PORTD)
#define LCD_DPin         DDRD	// Apibreziami 4-bitu isvadai (PD4-PD7 at PORT D)
#define LCD_DATA0_PIN    PD4    // < pin for 4bit data bit 0  
#define LCD_DATA1_PIN    PD5    // < pin for 4bit data bit 1  
#define LCD_DATA2_PIN    PD6    // < pin for 4bit data bit 2  
#define LCD_DATA3_PIN    PD7    // < pin for 4bit data bit 3  
#define RSPIN 			 PD3	// RS Pin
#define ENPIN            PD2 	// E Pin
int runtime;			 // Laiktrodis (timer) LCD ekranui

// LCD inicializavimo funkcija su minimaliu komandų sąrašu
void LCD_Init (void)
{
    // Duomenu ir kontrolės išvadų nustatymas kaip išejimo
    LCD_DPin |= (1<<LCD_DATA0_PIN)|(1<<LCD_DATA1_PIN)|(1<<LCD_DATA2_PIN)|(1<<LCD_DATA3_PIN)|(1<<RSPIN)|(1<<ENPIN);		//Kontroles LCD isvadai (D4-D7)
	_delay_ms(16);		// 16 ms vėlinimo funkcija
  
                            
	LCD_Action(0x02);	    // Gražina žymeklį į pradinę padėtį (Adresas 0). Gražina ekraną į pirminę būseną jeigu ji buvo pakeista 
	LCD_Action(0x28);       // Duomenų išsiuntimas/gavimas 4 bitu ilgiu (DB7-DB4) 
	LCD_Action(0x0C);       // Ekranas įjungtas, žymeklis išjungtas 
	LCD_Action(0x06);       // Žymeklio poslinkis į dešinę
	LCD_Action(0x01);       // Išvalomas LCD 
	_delay_ms(5);           // 5 ms vėlinimo funkcija
}

// Komandų ir duomenų siuntimo funkcija LCD ekranui
void LCD_Action( unsigned char cmnd )
{   
	LCD_Port = (LCD_Port & 0x0F) | (cmnd & 0xF0); 
	LCD_Port &= ~ (1<<RSPIN); 
	LCD_Port |= (1<<ENPIN); // Pateikiamas aukštas impulsas (LCD ON)
	_delay_us(1); // 1 ms vėlinimo funkcija
	LCD_Port &= ~ (1<<ENPIN); // Pateikiamas žemas impulsas (LCD OFF)
	_delay_us(200); // Laukiama 200 mikrosekundziu
	LCD_Port = (LCD_Port & 0x0F) | (cmnd << 4); //?
	LCD_Port |= (1<<ENPIN); // Pateikiamas aukštas impulsas (LCD ON)
	_delay_us(1);  // 1 ms vėlinimo funkcija
	LCD_Port &= ~ (1<<ENPIN); // Pateikiamas žemas impulsas (LCD OFF)
	_delay_ms(2);  // 2 ms vėlinimo funkcija
}

// Išvalomas visas LCD ekranas
void LCD_Clear()
{
	LCD_Action (0x01);		// Išvalomas LCD ekranas
	_delay_ms(2);			// 2 ms vėlinimo funkcija
	LCD_Action (0x80);		// Nustatoma pozicija į 1 eilutę ir 1 stulpelį
}

// Atvaizduojamos simbolių eilutės
void LCD_Print (char *str)
{
	int i;
	for(i=0; str[i]!=0; i++) // siunčiamas kiekvienas eilutės simbolis kol sąlyga tenkinama
	{
		LCD_Port = (LCD_Port & 0x0F) | (str[i] & 0xF0); 
		LCD_Port |= (1<<RSPIN); 
		LCD_Port|= (1<<ENPIN); // Pateikiamas aukštas impulsas (LCD ON)
		_delay_us(1); // 1 us vėlinimo funkcija
		LCD_Port &= ~ (1<<ENPIN); // Pateikiamas žemas impulsas (LCD OFF)
		_delay_us(200); // 200 us vėlinimo funkcija
		LCD_Port = (LCD_Port & 0x0F) | (str[i] << 4); 
		LCD_Port |= (1<<ENPIN); // Pateikiamas aukštas impulsas (LCD ON)
		_delay_us(1); // 1 us vėlinimo funkcija
		LCD_Port &= ~ (1<<ENPIN); // Pateikiamas žemas impulsas (LCD OFF)
		_delay_ms(2); // 2 ms vėlinimo funkcija
	}
}

// Atvaizduojami simboliai tam tikroje vietoje
// eilutė - 0 arba 1
// pozicija - stulpelis nuo 0 iki 16
void LCD_Printpos (char row, char pos, char *str)
{
	if (row == 0 && pos<16)
	LCD_Action((pos & 0x0F)|0x80); // Pirmos eilutės padėtis
	else if (row == 1 && pos<16)
	LCD_Action((pos & 0x0F)|0xC0); // Antros eilutės padėtis
	LCD_Print(str); // Atvaizduojama simbolių eilutė
}

// -----------  ADC functions -----------------------
// ADC inicializavimo funkcija
void InitADC()
{
  // Pasirenkamas įtampos šaltinis Vref=AVCC (5V) 
  // Pasirenkamas lygiavimas iš kairės
 ADMUX |= (1<<REFS0)|(1<<ADLAR); //|(1<<REFS0); 
  // Nustatomas daliklis 128 ir igalinamas ADC 
 ADCSRA |= (1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0)|(1<<ADEN);    
}

uint16_t ReadADC(uint8_t ADCchannel){
  // Pasirenkamas ADC kanalas su saugumo kauke
 ADMUX = (ADMUX & 0xF0) | (ADCchannel & 0x0F);  
  // Vienetinis konvertavimos režimas
 ADCSRA |= (1<<ADSC);
  // Laukiama kol ADC konvertavimas bus baigtas
 while( ADCSRA & (1<<ADSC) );
 return ADCH;
}

// ---------  Not LCD functions -----------------------


int main()
{
 	Serial.begin(9600); // Duomenų perdavimo sparta bit/s
  
    InitADC(); // Inicializuojama ADC funkcija
  
	LCD_Init();	// Inicializuojamas LCD ekranas

	LCD_Printpos(0, 0, "VilniusTech Menu");	// Atvaizduojami nurodyti simboliai pirmoje eilutėje
	LCD_Action(0xC0); // Perėjimas į antrą eilutę
 
  
  while(1) {
      double sensorInput = ReadADC(2); // Nuskaitomi ir išsaugomi duomenys iš analoginio daviklio
  		temp = (double)sensorInput / 256;   // Surandama įvesties reikšmės procentinė vertė 
  		temp = temp * 5; // Padauginama iš 5V, kad būtų gauta įtampa 
 		temp = temp - 0.5; // Atimamas poslinkis   
  		temp = temp * 100; // Konvertuojama į lapsnius  
    
    // Duomenų išvedimas į terminalą
    Serial.print("Temperatura: ");
    Serial.println(temp);
    LCD_Printpos(1, 0, "Temp1: "); // Atvaizduojami nurodyti simboliai antroje eilutėje
  	dtostrf(temp, 6, 2, buf); // Funkcija skirta konvertuoti float/double į char
        					  // temp - temperatūros reikšmė iš terminalo 
    						  // 6 - simbolių ilgis
    						  // 2 - simbolių kiekis po kablelio
    						  // buf - char tipo temperatūros reikšmės
    LCD_Print(buf); // Atvaizduojama termperatūros vertė LCD ekrane
    LCD_Print(" oC"); // Atvaizduojamas Celsijaus laipsnių ženklas LCD ekrane
   // _delay_ms(250); // 250 ms vėlinimo funkcija

	}
}